from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response
from starlette.routing import Route
from tortoise.exceptions import DoesNotExist

from ..db import Account, Transaction
from .utils import JSONResponse, with_json_params


def get_routes() -> list[Route]:
    return [
        Route("/", get_transactions, methods=["GET"]),
        Route("/add", create_transaction, methods=["POST"]),
        Route("/transfer", transfer, methods=["POST"]),
    ]


async def get_transactions(req: Request) -> Response:
    transactions = await Transaction.filter(
        account__user_=req.scope["user"].username
    ).all()
    return JSONResponse([dict(transaction) for transaction in transactions])


@with_json_params(args=["account", "item", "category", "amount"])
async def create_transaction(
    req: Request, account: str, item: str, category: str, amount: float
) -> Response:
    try:
        account_ = await Account.get(
            user_=req.scope["user"].username, name=account
        )
    except DoesNotExist:
        return PlainTextResponse(f"The account {account} doesn't exist.", 400)

    amount_ = round(amount * 100)

    transaction = await Transaction.create(
        account=account_, item=item, category=category, amount=amount_
    )

    await account_.refresh_from_db(["amount"])

    return JSONResponse(
        {
            "transaction": dict(transaction),
            "account": dict(account_),
        }
    )


@with_json_params(args=["account_from", "account_to", "amount"])
async def transfer(
    req: Request, account_from: str, account_to: str, amount: float
) -> Response:
    try:
        account_from_: Account = await Account.get(
            user_=req.scope["user"].username, name=account_from
        )
    except DoesNotExist:
        return PlainTextResponse(
            f"The account {account_from} doesn't exist.", 400
        )
    try:
        account_to_: Account = await Account.get(
            user_=req.scope["user"].username, name=account_to
        )
    except DoesNotExist:
        return PlainTextResponse(
            f"The account {account_to} doesn't exist.", 400
        )

    amount_ = round(amount * 100)

    transactions = await Transaction.create_transfer(
        account_from=account_from_, account_to=account_to_, amount=amount_
    )

    return JSONResponse(
        {
            "transactions": (dict(transactions[0]), dict(transactions[1])),
            "accounts": (dict(account_from_), dict(account_to_)),
        }
    )
