from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response
from starlette.routing import Route
from tortoise.exceptions import DoesNotExist, IntegrityError

from ..db import Account, AccountHistory, Transaction
from .utils import JSONResponse, with_json_params, with_query_string_params


def get_routes() -> list[Route]:
    return [
        Route("/", get_accounts, methods=["GET"]),
        Route("/get", get_account, methods=["GET"]),
        Route("/add", add_account, methods=["POST"]),
        Route("/update", update_account, methods=["POST"]),
    ]


async def get_accounts(req: Request) -> Response:
    accounts = [
        dict(account)
        for account in await Account.filter(
            user_=req.scope["user"].username
        ).all()
    ]
    for account in accounts:
        account["history"] = [
            dict(hist)
            for hist in await AccountHistory.filter(
                account=account["id"]
            ).all()
        ]
    return JSONResponse(accounts)


@with_query_string_params(args=["account"])
async def get_account(req: Request, account: str) -> Response:
    try:
        account_ = await Account.get(
            user_=req.scope["user"].username, name=account
        )
        history = await AccountHistory.filter(account=account_).all()
        return JSONResponse(
            {**dict(account_), "history": [dict(hist) for hist in history]}
        )
    except DoesNotExist:
        return PlainTextResponse(f"The account {account} doesn't exist.", 400)


@with_json_params(
    args=["name", "is_credit", "amount"],
)
async def add_account(
    req: Request,
    name: str,
    is_credit: bool,
    amount: float,
) -> Response:
    try:
        account = await Account.create(
            user_=req.scope["user"].username,
            name=name,
            is_credit=is_credit,
            amount=int(amount * 100),
        )
    except IntegrityError:
        return PlainTextResponse(
            f"There already is an account named {name}.", 400
        )

    return JSONResponse({**dict(account), "history": []})


@with_json_params(
    args=["account", "amount"],
)
async def update_account(
    req: Request,
    account: str,
    amount: float,
) -> Response:
    try:
        account_ = await Account.get(
            user_=req.scope["user"].username, name=account
        )
    except DoesNotExist:
        return PlainTextResponse(f"The account {account} doesn't exist.", 400)

    amount_ = round(amount * 100)

    amount_ = (amount_ - account_.amount) * (
        -1 if account_.is_credit else 1  # type: ignore
    )

    transaction = await Transaction.create(
        account=account_,
        item="adjustment",
        category="adjustment",
        amount=amount_,
    )

    await account_.refresh_from_db(["amount"])

    await AccountHistory.create(account_)
    history = (
        await AccountHistory.filter(account=account_).order_by("date").all()
    )

    return JSONResponse(
        {
            "account": {
                **dict(account_),
                "history": [dict(hist) for hist in history],
            },
            "transaction": dict(transaction),
        }
    )
