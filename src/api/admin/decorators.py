from typing import Any, Awaitable, Callable

from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response


def admin_required(
    fct: Callable[..., Awaitable[Response]]
) -> Callable[..., Awaitable[Response]]:
    async def wrapper(req: Request, *args: Any, **kwargs: Any) -> Response:
        try:
            user = req.scope["user"]
        except KeyError:
            return PlainTextResponse(
                "You don't have access to this resource.", status_code=401
            )
        if not user.is_admin:
            return PlainTextResponse(
                "You don't have access to this resource.", status_code=401
            )
        return await fct(req, *args, **kwargs)

    return wrapper
