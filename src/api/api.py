import re
from pathlib import Path

from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response
from starlette.routing import BaseRoute, Mount, Route

from . import accounts, admin, transactions

pyproject_path = Path(__file__).parent / ".." / ".." / "pyproject.toml"


def get_routes() -> list[BaseRoute]:
    return [
        Route("/", endpoint=health_check, methods=["GET"]),
        Route("/version", endpoint=get_version, methods=["GET"]),
        Mount("/admin", routes=admin.get_routes()),
        Mount("/accounts", routes=accounts.get_routes()),
        Mount("/transactions", routes=transactions.get_routes()),
    ]


async def health_check(_: Request) -> Response:
    return PlainTextResponse("Healthy!")


async def get_version(_: Request) -> Response:
    with open(pyproject_path) as f:
        for line in f:
            match = re.match(r'^version\s*=\s*"(.+)"$', line.strip())
            if match is not None:
                version = match.group(1)
                return PlainTextResponse(version)
    return PlainTextResponse("Unknown version", 500)
