from typing import Awaitable, Callable

import jwt
import jwt.exceptions
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response

from . import config
from .db import User


def verify_token(token: str) -> User:
    """
    Returns the user corresponding to the provided token. If refresh
    token, it will be validated agains the blacklisted tokens.

    Parameters
    ----------
    db : Session
        Database instance
    token : str
        Token to validate

    Returns
    -------
    User
        User corresponding to the token

    Raises
    ------
    ValueError
        If the token isn't valid
    """
    key = str(config.TOKEN_KEY)
    try:
        payload = jwt.decode(
            token, key, audience=config.TOKEN_AUD, algorithms=["HS256"]
        )
    except jwt.exceptions.ExpiredSignatureError as exc:
        raise ValueError("The token expired.") from exc
    except Exception as exc:
        raise ValueError("The token is invalid.") from exc

    try:
        user = User(username=payload["username"], is_admin=payload["is_admin"])
    except KeyError as exc:
        raise ValueError("The token is invalid.") from exc

    return user


class AuthMiddleware(BaseHTTPMiddleware):
    async def dispatch(
        self: "AuthMiddleware",
        req: Request,
        call_next: Callable[[Request], Awaitable[Response]],
    ) -> Response:
        whitelisted = [
            f"/{path}" if not path.startswith("/") else path
            for path in config.WHITELISTED_URIS
        ]
        if req["path"] in whitelisted or req.method == "OPTIONS":
            return await call_next(req)

        access_token = req.headers.get("access-token")

        if access_token is None:
            return PlainTextResponse(
                "You don't have access to this resource.", status_code=401
            )

        try:
            user = verify_token(access_token)
        except ValueError as exc:
            if str(exc) == "The token expired.":
                return PlainTextResponse("The token expired.", status_code=401)
            else:
                return PlainTextResponse(
                    "You don't have access to this resource.", status_code=401
                )

        req.scope["user"] = user
        return await call_next(req)
