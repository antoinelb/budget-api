from datetime import datetime
from typing import Any, Iterator

from tortoise import fields
from tortoise.models import Model

from src.db.accounts import Account


class Transaction(Model):

    id = fields.IntField(pk=True)
    account = fields.ForeignKeyField(
        "models.Account", related_name="transactions"
    )
    item = fields.CharField(32)
    category = fields.CharField(32)
    amount = fields.BigIntField()
    date = fields.DateField()
    created_on = fields.DatetimeField(auto_now_add=True)
    last_modified = fields.DatetimeField(auto_now=True)

    def __str__(self) -> str:
        return f"<transaction {self.id} for {self.account}>"

    def __iter__(self) -> Iterator[tuple[str, Any]]:
        return (
            (field.replace("_id", ""), val / 100 if field == "amount" else val)
            for field, val in self.__dict__.items()
            if not field.startswith("_")
        )

    @classmethod
    async def create(  # type: ignore
        cls,
        account: Account,
        item: str,
        category: str,
        amount: int,
    ) -> "Transaction":
        transaction = await super(Transaction, cls).create(
            account=account,
            item=item,
            category=category,
            amount=amount,
            date=datetime.utcnow().date(),
        )

        amount = -amount if account.is_credit else amount  # type: ignore

        await account.update_from_dict({"amount": account.amount + amount})
        await account.save()

        return transaction

    @classmethod
    async def create_transfer(
        cls,
        account_from: Account,
        account_to: Account,
        amount: int,
    ) -> tuple["Transaction", "Transaction"]:
        date_ = datetime.utcnow().date()

        amount_from = -amount * (
            -1 if account_from.is_credit else 1  # type: ignore
        )
        amount_to = amount * (
            -1 if account_to.is_credit else 1  # type: ignore
        )

        transactions = (
            await super(Transaction, cls).create(
                account=account_from,
                item="transfer",
                category="transfer",
                amount=amount_from,
                date=date_,
            ),
            await super(Transaction, cls).create(
                account=account_to,
                item="transfer",
                category="transfer",
                amount=amount_to,
                date=date_,
            ),
        )
        await account_from.update_from_dict(
            {"amount": account_from.amount + amount_from}
        )
        await account_from.save()
        await account_to.update_from_dict(
            {"amount": account_to.amount + amount_to}
        )
        await account_to.save()

        return transactions
