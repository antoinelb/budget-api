import datetime
from typing import Any, Iterator, Optional

from tortoise import fields
from tortoise.exceptions import IntegrityError
from tortoise.models import Model


class Account(Model):
    id = fields.IntField(pk=True)
    user_ = fields.CharField(64, index=True)
    name = fields.CharField(32)
    is_credit = fields.BooleanField()
    amount = fields.BigIntField()
    created_on = fields.DatetimeField(auto_now_add=True)
    last_modified = fields.DatetimeField(auto_now=True)

    class Meta:
        unique_together = ("user_", "name")

    def __str__(self) -> str:
        return f"<account {self.name} for user {self.user_}>"

    def __iter__(self) -> Iterator[tuple[str, Any]]:
        return (
            (field.replace("_id", ""), val / 100 if field == "amount" else val)
            for field, val in self.__dict__.items()
            if not field.startswith("_")
        )


class AccountHistory(Model):
    id = fields.IntField(pk=True)
    account = fields.ForeignKeyField("models.Account", related_name="history")
    amount = fields.BigIntField()
    date = fields.DateField()
    created_on = fields.DatetimeField(auto_now_add=True)
    last_modified = fields.DatetimeField(auto_now=True)

    class Meta:
        unique_together = ("account", "date")

    def __str__(self) -> str:
        return f"<account history on {self.date} for {self.account}>"

    def __iter__(self) -> Iterator[tuple[str, Any]]:
        return (
            (field.replace("_id", ""), val / 100 if field == "amount" else val)
            for field, val in self.__dict__.items()
            if not field.startswith("_")
        )

    @classmethod
    async def create(  # type: ignore
        cls, account: Account, date_: Optional[datetime.date] = None
    ) -> "AccountHistory":
        if date_ is None:
            date_ = datetime.datetime.utcnow().date()
        try:
            history = await super(AccountHistory, cls).create(
                account=account, amount=account.amount, date=date_
            )
        except IntegrityError:
            history = await super(AccountHistory, cls).get(
                account=account, date=str(date_)
            )
            await history.update_from_dict({"amount": account.amount})
            await history.save()

        return history
