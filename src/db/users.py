from dataclasses import dataclass
from typing import Any, Iterator


@dataclass
class User:
    username: str
    is_admin: bool

    def __str__(self) -> str:
        if self.is_admin:
            return f"<admin user {self.username}>"
        else:
            return f"<user {self.username}>"

    def __iter__(self) -> Iterator[tuple[str, Any]]:
        return iter(self.__dict__.items())
