from tortoise import Tortoise

from .. import config
from .accounts import Account
from .transactions import Transaction

db_url = (
    f"postgres://{config.DB_USER}:{config.DB_PASS}"
    f"@{config.DB_HOST}:{config.DB_PORT}/{config.DB_NAME}"
)
modules = {
    "models": [
        "aerich.models",
        "src.db.accounts",
        "src.db.transactions",
    ]
}
db_config = {
    "connections": {"default": db_url},
    "apps": {
        "models": {
            **{
                "default_connection": "default",
            },
            **modules,  # type: ignore
        }
    },
    "use_tz": False,
}


async def init_db() -> None:
    await Tortoise.init(db_url=db_url, modules=modules)
    await Tortoise.generate_schemas()


async def close_db() -> None:
    await Tortoise.close_connections()


async def empty_db() -> None:
    await Account.all().delete()
    await Transaction.all().delete()
