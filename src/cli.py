import typer

from . import app


def init_cli() -> typer.Typer:
    cli = typer.Typer()
    cli.command("run")(run)
    cli.command("r", hidden=True)(run)
    return cli


def run_cli() -> None:
    cli = init_cli()
    cli()


def run() -> None:
    app.run_server()
