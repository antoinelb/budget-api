import os

from starlette.config import Config
from starlette.datastructures import CommaSeparatedStrings, Secret

config = Config(os.path.join(os.path.dirname(__file__), os.pardir, ".env"))

DEBUG = config("DEBUG", cast=bool, default=False)
RELOAD = config("RELOAD", cast=bool, default=False)

DB_HOST = config("DB_HOST", default="127.0.0.1")
DB_PORT = config("DB_PORT", cast=int, default="5432")
DB_NAME = config("DB_NAME")
DB_PASS = config("DB_PASS")
DB_USER = config("DB_USER")

HOST = config("HOST", default="127.0.0.1")
PORT = config("PORT", cast=int, default=8000)
DOMAIN = config("DOMAIN")
TOKEN_AUD = config("TOKEN_AUD")
TOKEN_KEY = config("TOKEN_KEY", cast=Secret)

WHITELISTED_URIS = config(
    "WHITELISTED_URIS",
    cast=CommaSeparatedStrings,
    default=["", "version"],
)
