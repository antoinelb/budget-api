# Authentication and authorization api

## Initial setup

1. Create postgresql db `DB_NAME`

```
createdb $DB_NAME
```

2. Create user `DB_USER`

```
psql $DB_NAME -c "create user $DB_USER with password $DB_PASS"
```

3. Grant privileges to user

```
psql $DB_NAME -c "grant all privileges on database $DB_NAME to $DB_USER"
```

4. If you are going to run tests, make sure the database and user corresponding to the variables in `setup.py` (by default `test_budget` and `app`) are also created.
