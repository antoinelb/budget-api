-- upgrade --
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(20) NOT NULL,
    "content" JSONB NOT NULL
);
CREATE TABLE IF NOT EXISTS "account" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "user_" VARCHAR(64) NOT NULL,
    "name" VARCHAR(32) NOT NULL,
    "is_credit" BOOL NOT NULL,
    "amount" BIGINT NOT NULL,
    "created_on" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "last_modified" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT "uid_account_user__012881" UNIQUE ("user_", "name")
);
CREATE INDEX IF NOT EXISTS "idx_account_user__458f6e" ON "account" ("user_");
CREATE TABLE IF NOT EXISTS "transaction" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "item" VARCHAR(32) NOT NULL,
    "category" VARCHAR(32) NOT NULL,
    "amount" BIGINT NOT NULL,
    "date" DATE NOT NULL,
    "created_on" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "last_modified" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "account_id" INT NOT NULL REFERENCES "account" ("id") ON DELETE CASCADE
);
