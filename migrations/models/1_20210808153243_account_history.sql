-- upgrade --
CREATE TABLE IF NOT EXISTS "accounthistory" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "amount" BIGINT NOT NULL,
    "date" DATE NOT NULL,
    "created_on" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "last_modified" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "account_id" INT NOT NULL REFERENCES "account" ("id") ON DELETE CASCADE
);
-- downgrade --
DROP TABLE IF EXISTS "accounthistory";
