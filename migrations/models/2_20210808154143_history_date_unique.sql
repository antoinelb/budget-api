-- upgrade --
CREATE UNIQUE INDEX "uid_accounthist_account_110687" ON "accounthistory" ("account_id", "date");
-- downgrade --
DROP INDEX "uid_accounthist_account_110687";
