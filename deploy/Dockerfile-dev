FROM python:3.9-alpine as base

RUN apk update && \
    apk add --no-cache \
      libffi \
      libstdc++ \
      openssl \
      postgresql-dev \
      python3


FROM base as builder

WORKDIR /install

RUN apk update && \
    apk add --no-cache \
      build-base \
      cargo \
      git \
      libffi-dev \
      openssl-dev \
      python3-dev \
      rust

RUN pip3 install --upgrade pip && \
    pip3 install poetry

COPY ./pyproject.toml /install/

RUN python3 -m venv /venv && \
    . /venv/bin/activate && \
    pip install --upgrade pip && \
    poetry install


FROM base

WORKDIR /app

ENV PATH="/venv/bin:$PATH"

COPY --from=builder /venv /venv

COPY . /app
