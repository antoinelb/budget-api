from datetime import date, datetime, timedelta

import pytest

from src.db import Account, AccountHistory


async def test_account__str(debit_account: Account) -> None:
    assert (
        str(debit_account)
        == f"<account {debit_account.name} for user {debit_account.user_}>"
    )


async def test_account__dict(
    debit_account: Account,
) -> None:
    account_ = dict(debit_account)
    for field in (
        "id",
        "user_",
        "name",
        "is_credit",
        "created_on",
        "last_modified",
    ):
        assert account_[field] == getattr(debit_account, field)
    assert account_["amount"] * 100 == debit_account.amount
    assert len(account_) == 7


async def test_account_history__str(
    debit_account_history: list[AccountHistory], debit_account: Account
) -> None:
    assert (
        str(debit_account_history[0])
        == f"<account history on {debit_account_history[0].date} for {debit_account}>"  # noqa
    )


async def test_account_history__dict(
    debit_account_history: list[AccountHistory],
) -> None:
    history = dict(debit_account_history[0])
    for field in (
        "id",
        "date",
        "created_on",
        "last_modified",
    ):
        assert history[field] == getattr(debit_account_history[0], field)
    assert history["account"] == debit_account_history[0].account.id  # type: ignore # noqa
    assert history["amount"] * 100 == debit_account_history[0].amount
    assert len(history) == 6


@pytest.mark.parametrize(
    "date_", [None, datetime.utcnow().date() + timedelta(days=-1)]
)
async def test_transaction__create(
    debit_account: Account, date_: date
) -> None:
    if date_ is None:
        history = await AccountHistory.create(debit_account)
    else:
        history = await AccountHistory.create(debit_account, date_)
    assert history.amount == debit_account.amount
    if date_ is None:
        assert history.date == datetime.utcnow().date()
    else:
        assert history.date == date_


async def test_transaction__create__already_exists(
    debit_account_history: list[AccountHistory],
) -> None:
    amount = debit_account_history[0].amount
    await debit_account_history[0].account.update_from_dict(  # type: ignore
        {"amount": amount + 100}
    )
    await debit_account_history[0].account.save()  # type: ignore
    history = await AccountHistory.create(
        debit_account_history[0].account  # type: ignore
    )
    assert history.amount == amount + 100
    assert history.date == datetime.utcnow().date()
