from src.db import User


async def test_user__str(user: User, admin: User) -> None:
    assert str(user) == f"<user {user.username}>"
    assert str(admin) == f"<admin user {admin.username}>"


async def test_user__dict(user: User) -> None:
    user_ = dict(user)
    for field in (
        "username",
        "is_admin",
    ):
        assert user_[field] == getattr(user, field)
    assert len(user_) == 2
