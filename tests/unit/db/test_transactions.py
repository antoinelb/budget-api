from datetime import datetime

from src.db import Account, Transaction


async def test_transaction__str(transaction: Transaction) -> None:
    assert (
        str(transaction)
        == f"<transaction {transaction.id} for {transaction.account}>"
    )


async def test_transaction__dict(
    transaction: Transaction,
) -> None:
    transaction_ = dict(transaction)
    for field in (
        "id",
        "item",
        "category",
        "date",
        "created_on",
        "last_modified",
    ):
        assert transaction_[field] == getattr(transaction, field)
    assert transaction_["account"] == transaction.account.id  # type: ignore
    assert transaction_["amount"] * 100 == transaction.amount
    assert len(transaction_) == 8


async def test_transaction__create__single_debit(
    debit_account: Account,
) -> None:
    original_amount = debit_account.amount
    item = "item"
    category = "category"
    amount = -1000

    transaction = await Transaction.create(
        account=debit_account, item=item, category=category, amount=amount
    )
    assert isinstance(transaction, Transaction)
    await debit_account.refresh_from_db(["amount"])
    assert debit_account.amount == original_amount + amount
    assert transaction.date == datetime.utcnow().date()

    transaction = await Transaction.create(
        account=debit_account, item=item, category=category, amount=amount
    )
    assert isinstance(transaction, Transaction)
    await debit_account.refresh_from_db(["amount"])
    assert debit_account.amount == original_amount + amount * 2
    assert transaction.date == datetime.utcnow().date()


async def test_transaction__create__single_credit(
    credit_account: Account,
) -> None:
    original_amount = credit_account.amount
    item = "item"
    category = "category"
    amount = -1000

    transaction = await Transaction.create(
        account=credit_account, item=item, category=category, amount=amount
    )
    assert isinstance(transaction, Transaction)
    await credit_account.refresh_from_db(["amount"])
    assert credit_account.amount == original_amount - amount
    assert transaction.date == datetime.utcnow().date()

    transaction = await Transaction.create(
        account=credit_account, item=item, category=category, amount=amount
    )
    assert isinstance(transaction, Transaction)
    await credit_account.refresh_from_db(["amount"])
    assert credit_account.amount == original_amount - amount * 2
    assert transaction.date == datetime.utcnow().date()


async def test_transaction__create_transfer__debit_to_debit(
    debit_account: Account, other_debit_account: Account
) -> None:
    original_from = debit_account.amount
    original_to = other_debit_account.amount
    amount = 1000

    transactions = await Transaction.create_transfer(
        debit_account, other_debit_account, amount
    )
    assert transactions[0].item == transactions[1].item == "transfer"
    assert transactions[0].category == transactions[1].category == "transfer"
    assert (
        transactions[0].date
        == transactions[1].date
        == datetime.utcnow().date()
    )

    await debit_account.refresh_from_db(["amount"])
    await other_debit_account.refresh_from_db(["amount"])
    assert debit_account.amount == original_from - amount
    assert other_debit_account.amount == original_to + amount


async def test_transaction__create_transfer__debit_to_credit(
    debit_account: Account, credit_account: Account
) -> None:
    original_from = debit_account.amount
    original_to = credit_account.amount
    amount = 1000

    transactions = await Transaction.create_transfer(
        debit_account, credit_account, amount
    )
    assert transactions[0].item == transactions[1].item == "transfer"
    assert transactions[0].category == transactions[1].category == "transfer"
    assert (
        transactions[0].date
        == transactions[1].date
        == datetime.utcnow().date()
    )

    await debit_account.refresh_from_db(["amount"])
    await credit_account.refresh_from_db(["amount"])
    assert debit_account.amount == original_from - amount
    assert credit_account.amount == original_to - amount


async def test_transaction__create_transfer__credit_to_debit(
    credit_account: Account, debit_account: Account
) -> None:
    original_from = credit_account.amount
    original_to = debit_account.amount
    amount = 1000

    transactions = await Transaction.create_transfer(
        credit_account, debit_account, amount
    )
    assert transactions[0].item == transactions[1].item == "transfer"
    assert transactions[0].category == transactions[1].category == "transfer"
    assert (
        transactions[0].date
        == transactions[1].date
        == datetime.utcnow().date()
    )

    await credit_account.refresh_from_db(["amount"])
    await debit_account.refresh_from_db(["amount"])
    assert credit_account.amount == original_from + amount
    assert debit_account.amount == original_to + amount


async def test_transaction__create_transfer__credit_to_credit(
    credit_account: Account, other_credit_account: Account
) -> None:
    original_from = credit_account.amount
    original_to = other_credit_account.amount
    amount = 1000

    transactions = await Transaction.create_transfer(
        credit_account, other_credit_account, amount
    )
    assert transactions[0].item == transactions[1].item == "transfer"
    assert transactions[0].category == transactions[1].category == "transfer"
    assert (
        transactions[0].date
        == transactions[1].date
        == datetime.utcnow().date()
    )

    await credit_account.refresh_from_db(["amount"])
    await other_credit_account.refresh_from_db(["amount"])
    assert credit_account.amount == original_from + amount
    assert other_credit_account.amount == original_to - amount
