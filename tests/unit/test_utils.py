import pytest

from src.utils import format_list


@pytest.mark.parametrize(
    "items,correct",
    [
        ([], ""),
        (["a"], "a"),
        (["a", "b"], "a and b"),
        (["a", "b", "c"], "a, b and c"),
    ],
)
def test_format_list(items: list[str], correct: str) -> None:
    assert format_list(items) == correct
