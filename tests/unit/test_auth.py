from datetime import datetime, timedelta
from typing import Dict

import jwt
import pytest
from pytest_mock import MockerFixture
from starlette.testclient import TestClient

from src import config
from src.auth import verify_token
from src.db import User


async def test_verify_token(
    user: User,
    access_token: str,
) -> None:
    user_ = verify_token(access_token)
    assert user == user_


async def test_verify_token__wrong_payload() -> None:
    token = jwt.encode(
        {
            "aud": config.TOKEN_AUD,
            "iat": datetime.utcnow(),
            "exp": datetime.utcnow() + timedelta(minutes=1),
        },
        str(config.TOKEN_KEY),
    )
    with pytest.raises(ValueError) as exp:
        verify_token(token)
    assert str(exp.value) == "The token is invalid."


async def test_verify_token__wrong_format() -> None:
    with pytest.raises(ValueError) as exp:
        verify_token("wrong")
    assert str(exp.value) == "The token is invalid."


async def test_verify_token__expired_token() -> None:
    token = jwt.encode(
        {
            "username": "test",
            "aud": config.TOKEN_AUD,
            "iat": datetime.utcnow(),
            "exp": datetime.utcnow() - timedelta(minutes=1),
        },
        str(config.TOKEN_KEY),
    )
    with pytest.raises(ValueError) as exp:
        verify_token(token)
    assert str(exp.value) == "The token expired."


async def test_auth_middleware__not_secure(client: TestClient) -> None:
    resp = client.get("/")
    assert resp.status_code == 200
    assert resp.text == "Healthy!"


async def test_auth_middleware__secure__passes(
    client: TestClient, auth_headers: Dict[str, str], user: User
) -> None:
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 200
    assert resp.text == f"Secure pong for user {user.username}!"


async def test_auth_middleware__secure__missing_access(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    del auth_headers["access-token"]
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 401
    assert resp.text == "You don't have access to this resource."


async def test_auth_middleware__secure__invalid_access_token(
    mocker: MockerFixture,
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    mocker.patch("src.auth.verify_token", side_effect=ValueError())
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 401
    assert resp.text == "You don't have access to this resource."


async def test_auth_middleware__secure__expired_access_token(
    mocker: MockerFixture,
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    mocker.patch(
        "src.auth.verify_token",
        side_effect=ValueError("The token expired."),
    )
    resp = client.get("/secure", headers=auth_headers)
    assert resp.status_code == 401
    assert resp.text == "The token expired."
