import json
from datetime import date, datetime, timezone
from typing import Any, Dict, Optional
from unittest.mock import MagicMock

import pytest
from pytest_mock import MockerFixture
from starlette.responses import Response

from src.api.utils import (
    JSONResponse,
    convert_datetimes_for_json,
    get_headers,
    get_json_params,
    get_path_params,
    get_query_string_params,
    with_headers,
    with_json_params,
    with_path_params,
    with_query_string_params,
)
from tests.utils import as_async_fct


async def test_get_json_params(mocker: MockerFixture) -> None:
    data = {"a": 1, "b": 2, "c": 3, "d": 4}

    async def body() -> str:
        return json.dumps(data)

    req = mocker.Mock()
    req.body = body

    args = await get_json_params(
        req, args=["a", "b"], opt_args=["c", "d", "e"]
    )

    assert not isinstance(args, Response)
    assert args == data


async def test_get_json_params__no_args(mocker: MockerFixture) -> None:
    data: Dict[str, str] = {}

    async def body() -> str:
        return json.dumps(data)

    req = mocker.Mock()
    req.body = body

    args = await get_json_params(req)

    assert not isinstance(args, Response)
    assert args == {}


async def test_get_json_params__wrong_type(mocker: MockerFixture) -> None:
    async def body() -> str:
        return "test"

    req = mocker.Mock()
    req.body = body

    args = await get_json_params(req)

    assert isinstance(args, Response)
    assert args.body.decode() == "Wrong data type was sent."


async def test_get_json_params__missing_args(mocker: MockerFixture) -> None:
    data = {"a": 1}

    async def body() -> str:
        return json.dumps(data)

    req = mocker.Mock()
    req.body = body

    args = await get_json_params(req, args=["a", "b"])

    assert isinstance(args, Response)
    assert args.body.decode() == "There are missing parameters."


async def test_get_query_string_params(mocker: MockerFixture) -> None:
    data = {"a": 1, "b": 2, "c": 3, "d": 4}

    req = mocker.Mock()
    req.query_params = data

    args = await get_query_string_params(
        req, args=["a", "b"], opt_args=["c", "d", "e"]
    )

    assert not isinstance(args, Response)
    assert args == data


async def test_get_query_string_params__no_args(mocker: MockerFixture) -> None:
    data: Dict[str, str] = {}

    req = mocker.Mock()
    req.query_params = data

    args = await get_query_string_params(req)

    assert not isinstance(args, Response)
    assert args == {}


async def test_get_query_string_params__missing_args(
    mocker: MockerFixture,
) -> None:
    data = {"a": 1}

    req = mocker.Mock()
    req.query_params = data

    args = await get_query_string_params(req, args=["a", "b"])

    assert isinstance(args, Response)
    assert args.body.decode() == "There are missing parameters."


async def test_get_path_params(mocker: MockerFixture) -> None:
    data = {"a": 1, "b": 2, "c": 3, "d": 4}

    req = mocker.Mock()
    req.path_params = data

    args = await get_path_params(
        req, args=["a", "b"], opt_args=["c", "d", "e"]
    )

    assert not isinstance(args, Response)
    assert args == data


async def test_get_path_params__no_args(mocker: MockerFixture) -> None:
    data: Dict[str, str] = {}

    req = mocker.Mock()
    req.path_params = data

    args = await get_path_params(req)

    assert not isinstance(args, Response)
    assert args == {}


async def test_get_path_params__missing_args(mocker: MockerFixture) -> None:
    data = {"a": 1}

    req = mocker.Mock()
    req.path_params = data

    args = await get_path_params(req, args=["a", "b"])

    assert isinstance(args, Response)
    assert args.body.decode() == "There are missing parameters."


async def test_get_headers(mocker: MockerFixture) -> None:
    data = {"a": 1, "b": 2, "c": 3, "d": 4}

    req = mocker.Mock()
    req.headers = data

    args = await get_headers(req, args=["a", "b"], opt_args=["c", "d", "e"])

    assert not isinstance(args, Response)
    assert args == data


async def test_get_headers__no_args(mocker: MockerFixture) -> None:
    data: Dict[str, str] = {}

    req = mocker.Mock()
    req.headers = data

    args = await get_headers(req)

    assert not isinstance(args, Response)
    assert args == {}


async def test_get_headers__missing_args(mocker: MockerFixture) -> None:
    data = {"a": 1}

    req = mocker.Mock()
    req.headers = data

    args = await get_headers(req, args=["a", "b"])

    assert isinstance(args, Response)
    assert args.body.decode() == "There are missing headers."


async def test_with_json_params(mocker: MockerFixture) -> None:
    get_json_params = mocker.patch(
        "src.api.utils.get_json_params",
        side_effect=as_async_fct(
            MagicMock(
                return_value=({"a": 1, "b-b": 2, "c": 3, "d": 4, "e": 5})
            )
        ),
    )

    async def test(
        req: Any,  # pylint: disable=unused-argument
        a: int,  # pylint: disable=unused-argument
        b_b: int,  # pylint: disable=unused-argument
        c: int,  # pylint: disable=unused-argument
        d: Optional[int] = None,  # pylint: disable=unused-argument
        e: Optional[int] = None,  # pylint: disable=unused-argument
    ) -> None:
        return None

    decorator = with_json_params(args=["a", "b-b", "c"], opt_args=["d", "e"])
    fct = decorator(test)
    resp = await fct(None)
    assert resp is None
    assert await get_json_params.called_once()


async def test_with_json_params__missing_params(mocker: MockerFixture) -> None:
    get_json_params = mocker.patch(
        "src.api.utils.get_json_params",
        side_effect=as_async_fct(MagicMock(return_value=Response("test"))),
    )

    async def test(
        req: Any,  # pylint: disable=unused-argument
        a: int,  # pylint: disable=unused-argument
        b: int,  # pylint: disable=unused-argument
        c: int,  # pylint: disable=unused-argument
        d: Optional[int] = None,  # pylint: disable=unused-argument
        e: Optional[int] = None,  # pylint: disable=unused-argument
    ) -> None:
        return None

    decorator = with_json_params(args=["a", "b", "c"], opt_args=["d", "e"])
    fct = decorator(test)
    resp = await fct(None)
    assert isinstance(resp, Response)
    assert await get_json_params.called_once()


async def test_with_query_string_params(mocker: MockerFixture) -> None:
    get_query_string_params = mocker.patch(
        "src.api.utils.get_query_string_params",
        side_effect=as_async_fct(
            MagicMock(
                return_value=({"a": 1, "b-b": 2, "c": 3, "d": 4, "e": 5})
            )
        ),
    )

    async def test(
        req: Any,  # pylint: disable=unused-argument
        a: int,  # pylint: disable=unused-argument
        b_b: int,  # pylint: disable=unused-argument
        c: int,  # pylint: disable=unused-argument
        d: Optional[int] = None,  # pylint: disable=unused-argument
        e: Optional[int] = None,  # pylint: disable=unused-argument
    ) -> None:
        return None

    decorator = with_query_string_params(
        args=["a", "b-b", "c"], opt_args=["d", "e"]
    )
    fct = decorator(test)
    resp = await fct(None)
    assert resp is None
    assert await get_query_string_params.called_once()


async def test_with_query_string_params__missing_params(
    mocker: MockerFixture,
) -> None:
    get_query_string_params = mocker.patch(
        "src.api.utils.get_query_string_params",
        side_effect=as_async_fct(MagicMock(return_value=Response("test"))),
    )

    async def test(
        req: Any,  # pylint: disable=unused-argument
        a: int,  # pylint: disable=unused-argument
        b: int,  # pylint: disable=unused-argument
        c: int,  # pylint: disable=unused-argument
        d: Optional[int] = None,  # pylint: disable=unused-argument
        e: Optional[int] = None,  # pylint: disable=unused-argument
    ) -> None:
        return None

    decorator = with_query_string_params(
        args=["a", "b", "c"], opt_args=["d", "e"]
    )
    fct = decorator(test)
    resp = await fct(None)
    assert isinstance(resp, Response)
    assert await get_query_string_params.called_once()


async def test_with_path_params(mocker: MockerFixture) -> None:
    get_path_params = mocker.patch(
        "src.api.utils.get_path_params",
        side_effect=as_async_fct(
            MagicMock(
                return_value=({"a": 1, "b_b": 2, "c": 3, "d": 4, "e": 5})
            )
        ),
    )

    async def test(
        req: Any,  # pylint: disable=unused-argument
        a: int,  # pylint: disable=unused-argument
        b_b: int,  # pylint: disable=unused-argument
        c: int,  # pylint: disable=unused-argument
        d: Optional[int] = None,  # pylint: disable=unused-argument
        e: Optional[int] = None,  # pylint: disable=unused-argument
    ) -> None:
        return None

    decorator = with_path_params(args=["a", "b_b", "c"], opt_args=["d", "e"])
    fct = decorator(test)
    resp = await fct(None)
    assert resp is None
    assert await get_path_params.called_once()


async def test_with_path_params__missing_params(mocker: MockerFixture) -> None:
    get_path_params = mocker.patch(
        "src.api.utils.get_path_params",
        side_effect=as_async_fct(MagicMock(return_value=Response("test"))),
    )

    async def test(
        req: Any,  # pylint: disable=unused-argument
        a: int,  # pylint: disable=unused-argument
        b: int,  # pylint: disable=unused-argument
        c: int,  # pylint: disable=unused-argument
        d: Optional[int] = None,  # pylint: disable=unused-argument
        e: Optional[int] = None,  # pylint: disable=unused-argument
    ) -> None:
        return None

    decorator = with_path_params(args=["a", "b", "c"], opt_args=["d", "e"])
    fct = decorator(test)
    resp = await fct(None)
    assert isinstance(resp, Response)
    assert await get_path_params.called_once()


async def test_with_headers(mocker: MockerFixture) -> None:
    get_headers = mocker.patch(
        "src.api.utils.get_headers",
        side_effect=as_async_fct(
            MagicMock(
                return_value=({"a": 1, "b_b": 2, "c": 3, "d": 4, "e": 5})
            )
        ),
    )

    async def test(
        req: Any,  # pylint: disable=unused-argument
        a: int,  # pylint: disable=unused-argument
        b_b: int,  # pylint: disable=unused-argument
        c: int,  # pylint: disable=unused-argument
        d: Optional[int] = None,  # pylint: disable=unused-argument
        e: Optional[int] = None,  # pylint: disable=unused-argument
    ) -> None:
        return None

    decorator = with_headers(args=["a", "b_b", "c"], opt_args=["d", "e"])
    fct = decorator(test)
    resp = await fct(None)
    assert resp is None
    assert await get_headers.called_once()


async def test_with_headers__missing_params(mocker: MockerFixture) -> None:
    get_headers = mocker.patch(
        "src.api.utils.get_headers",
        side_effect=as_async_fct(MagicMock(return_value=Response("test"))),
    )

    async def test(
        req: Any,  # pylint: disable=unused-argument
        a: int,  # pylint: disable=unused-argument
        b: int,  # pylint: disable=unused-argument
        c: int,  # pylint: disable=unused-argument
        d: Optional[int] = None,  # pylint: disable=unused-argument
        e: Optional[int] = None,  # pylint: disable=unused-argument
    ) -> None:
        return None

    decorator = with_headers(args=["a", "b", "c"], opt_args=["d", "e"])
    fct = decorator(test)
    resp = await fct(None)
    assert isinstance(resp, Response)
    assert await get_headers.called_once()


@pytest.mark.parametrize(
    "data",
    [
        1,
        1.0,
        "a",
        datetime(2020, 1, 1, 12, 0),
        [1, 1.0, "a", datetime(2020, 1, 1, 12, 0)],
        {1: 1, 2: 1.0, 3: "a", 4: datetime(2020, 1, 1, 12, 0)},
    ],
)
async def test_JSONResponse(data: Any) -> None:
    JSONResponse(data)


@pytest.mark.parametrize(
    "data,correct",
    [
        (1, 1),
        (1.0, 1.0),
        ("a", "a"),
        (
            datetime(2020, 1, 1, 12, 0),
            datetime(2020, 1, 1, 12, 0, tzinfo=timezone.utc).timestamp(),
        ),
        (
            [1, 1.0, "a", datetime(2020, 1, 1, 12, 0)],
            [
                1,
                1.0,
                "a",
                int(
                    datetime(
                        2020, 1, 1, 12, 0, tzinfo=timezone.utc
                    ).timestamp()
                ),
            ],
        ),
        (
            {1: 1, 2: 1.0, 3: "a", 4: datetime(2020, 1, 1, 12, 0)},
            {
                1: 1,
                2: 1.0,
                3: "a",
                4: int(
                    datetime(
                        2020, 1, 1, 12, 0, tzinfo=timezone.utc
                    ).timestamp()
                ),
            },
        ),
        (
            (1, 1.0, "a", date(2020, 1, 1)),
            (
                1,
                1.0,
                "a",
                int(
                    datetime(2020, 1, 1, 0, 0, tzinfo=timezone.utc).timestamp()
                ),
            ),
        ),
    ],
)
async def test_convert_datetimes_for_json(data: Any, correct: Any) -> None:
    assert convert_datetimes_for_json(data) == correct
