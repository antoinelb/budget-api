import json
from datetime import datetime
from typing import Dict

from starlette.testclient import TestClient

from src.api.utils import convert_datetimes_for_json
from src.db import Account, AccountHistory, Transaction, User
from tests.utils import equals


async def test_get_accounts(
    client: TestClient,
    auth_headers: Dict[str, str],
    debit_account: Account,
    credit_account: Account,
    other_debit_account: Account,
    debit_account_history: list[AccountHistory],
) -> None:
    accounts = [debit_account, credit_account]
    await other_debit_account.update_from_dict({"user_": "other"})
    await other_debit_account.save()

    resp = client.get("/accounts", headers=auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert all(
        [
            convert_datetimes_for_json(dict(account))
            == {k: v for k, v in account_.items() if k != "history"}
            for account, account_ in zip(accounts, data)
        ]
    )
    for account in data:
        if account["name"] == debit_account.name:
            assert len(account["history"]) == len(debit_account_history)
            assert all(
                hist_ == convert_datetimes_for_json(dict(hist))
                for hist_, hist in zip(
                    account["history"], debit_account_history
                )
            )
        else:
            assert account["history"] == []
    assert len(data) == 2


async def test_get_accounts__no_accounts(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.get("/accounts", headers=auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert not data


async def test_get_account(
    client: TestClient,
    auth_headers: Dict[str, str],
    debit_account: Account,
    debit_account_history: list[AccountHistory],
) -> None:
    resp = client.get(
        f"/accounts/get?account={debit_account.name}", headers=auth_headers
    )
    assert resp.status_code == 200
    data = resp.json()
    assert {
        k: v for k, v in data.items() if k != "history"
    } == convert_datetimes_for_json(dict(debit_account))
    assert len(data["history"]) == len(debit_account_history)
    assert all(
        hist == convert_datetimes_for_json(dict(hist_))
        for hist, hist_ in zip(data["history"], debit_account_history)
    )


async def test_get_account__unknown(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.get("/accounts/get?account=test", headers=auth_headers)
    assert resp.status_code == 400
    assert resp.text == "The account test doesn't exist."


async def test_add_account(
    client: TestClient,
    auth_headers: Dict[str, str],
    user: User,
) -> None:
    name = "test"
    is_credit = False
    amount = 100

    resp = client.post(
        "/accounts/add",
        headers=auth_headers,
        data=json.dumps(
            {
                "name": name,
                "is_credit": is_credit,
                "amount": amount,
            }
        ),
    )
    assert resp.status_code == 200
    data = resp.json()
    assert data["user_"] == user.username
    assert data["name"] == name
    assert data["is_credit"] == is_credit
    assert data["amount"] == amount
    assert data["history"] == []


async def test_add_account__already_exists(
    client: TestClient, auth_headers: Dict[str, str], debit_account: Account
) -> None:
    resp = client.post(
        "/accounts/add",
        headers=auth_headers,
        data=json.dumps(
            {
                "name": debit_account.name,
                "is_credit": debit_account.is_credit,
                "amount": 100,
            }
        ),
    )
    assert resp.status_code == 400
    assert (
        resp.text == f"There already is an account named {debit_account.name}."
    )


async def test_update_account(
    client: TestClient,
    auth_headers: Dict[str, str],
    debit_account: Account,
    credit_account: Account,
    debit_account_history: list[AccountHistory],
) -> None:
    amount = debit_account.amount / 100
    resp = client.post(
        "/accounts/update",
        headers=auth_headers,
        data=json.dumps(
            {
                "account": debit_account.name,
                "amount": amount + 100,
            }
        ),
    )
    assert resp.status_code == 200
    data = resp.json()
    account_ = data["account"]
    transaction_ = data["transaction"]
    await debit_account.refresh_from_db(["amount"])
    assert equals(
        {k: v for k, v in account_.items() if k != "history"},
        convert_datetimes_for_json(dict(debit_account)),
    )
    assert account_["amount"] == amount + 100
    assert datetime.utcfromtimestamp(
        account_["history"][-1]["date"]
    ) == datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0)
    assert account_["history"][-1]["account"] == account_["id"]
    assert (
        account_["history"][-1]["amount"]
        == account_["amount"]
        == debit_account.amount / 100
    )
    await AccountHistory.get(id=account_["history"][-1]["id"])
    transaction = await Transaction.get(id=transaction_["id"])
    assert transaction_["item"] == transaction.item == "adjustment"
    assert transaction_["category"] == transaction.category == "adjustment"
    assert transaction_["amount"] == transaction.amount / 100 == 100

    amount = credit_account.amount / 100
    resp = client.post(
        "/accounts/update",
        headers=auth_headers,
        data=json.dumps(
            {
                "account": credit_account.name,
                "amount": amount + 100,
            }
        ),
    )
    assert resp.status_code == 200
    data = resp.json()
    account_ = data["account"]
    transaction_ = data["transaction"]
    await credit_account.refresh_from_db(["amount"])
    assert equals(
        {k: v for k, v in account_.items() if k != "history"},
        convert_datetimes_for_json(dict(credit_account)),
    )
    assert account_["amount"] == amount + 100
    assert datetime.utcfromtimestamp(
        account_["history"][-1]["date"]
    ) == datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0)
    assert account_["history"][-1]["account"] == account_["id"]
    assert (
        account_["history"][-1]["amount"]
        == account_["amount"]
        == debit_account.amount / 100
    )
    await AccountHistory.get(id=account_["history"][-1]["id"])
    transaction = await Transaction.get(id=transaction_["id"])
    assert transaction_["item"] == transaction.item == "adjustment"
    assert transaction_["category"] == transaction.category == "adjustment"
    assert transaction_["amount"] == transaction.amount / 100 == -100


async def test_update_account__wrong_account(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/accounts/update",
        headers=auth_headers,
        data=json.dumps(
            {
                "account": "test",
                "amount": 100,
            }
        ),
    )
    assert resp.status_code == 400
    assert resp.text == "The account test doesn't exist."
