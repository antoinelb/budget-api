import json
from typing import Dict, List

from starlette.testclient import TestClient

from src.api.utils import convert_datetimes_for_json
from src.db import Account, Transaction


async def test_get_transactions(
    client: TestClient,
    auth_headers: Dict[str, str],
    transactions: List[Transaction],
) -> None:
    transactions[-1].account.user_ = "other"  # type: ignore
    await transactions[-1].account.save()  # type: ignore
    n = len([t for t in transactions if t.account != transactions[-1].account])
    resp = client.get("/transactions", headers=auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert all(
        [
            convert_datetimes_for_json(dict(transaction)) == transaction_
            for transaction, transaction_ in zip(transactions, data)
        ]
    )
    assert len(data) == n


async def test_get_transactions__no_transactions(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.get("/transactions", headers=auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert not data


async def test_create_transaction(
    client: TestClient,
    auth_headers: Dict[str, str],
    debit_account: Account,
) -> None:
    original_amount = debit_account.amount / 100
    item = "item"
    category = "category"
    amount = -10.0

    resp = client.post(
        "/transactions/add",
        headers=auth_headers,
        data=json.dumps(
            {
                "account": debit_account.name,
                "item": item,
                "category": category,
                "amount": amount,
            }
        ),
    )
    assert resp.status_code == 200
    data = resp.json()
    assert data["transaction"]["account"] == debit_account.id
    assert data["transaction"]["item"] == item
    assert data["transaction"]["category"] == category
    assert data["transaction"]["amount"] == amount
    assert data["account"]["amount"] == (original_amount + amount)

    await debit_account.refresh_from_db(["amount"])
    assert debit_account.amount / 100 == (original_amount + amount)


async def test_create_transaction__unknown_account(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    item = "item"
    category = "category"
    amount = -10.0

    resp = client.post(
        "/transactions/add",
        headers=auth_headers,
        data=json.dumps(
            {
                "account": "test",
                "item": item,
                "category": category,
                "amount": amount,
            }
        ),
    )
    assert resp.status_code == 400
    assert resp.text == "The account test doesn't exist."


async def test_transfer(
    client: TestClient,
    auth_headers: Dict[str, str],
    debit_account: Account,
    credit_account: Account,
) -> None:
    original_amount_from = debit_account.amount
    original_amount_to = debit_account.amount
    amount = 100

    resp = client.post(
        "/transactions/transfer",
        headers=auth_headers,
        data=json.dumps(
            {
                "account_from": debit_account.name,
                "account_to": credit_account.name,
                "amount": amount,
            }
        ),
    )
    assert resp.status_code == 200
    data = resp.json()
    assert len(data["transactions"]) == 2
    assert len(data["accounts"]) == 2
    assert data["accounts"][0]["amount"] == original_amount_from - amount * 100
    assert data["accounts"][1]["amount"] == original_amount_from - amount * 100

    await debit_account.refresh_from_db(["amount"])
    await credit_account.refresh_from_db(["amount"])
    assert debit_account.amount == original_amount_from - amount * 100
    assert credit_account.amount == original_amount_to - amount * 100


async def test_transfer__wrong_accounts(
    client: TestClient,
    auth_headers: Dict[str, str],
    debit_account: Account,
    credit_account: Account,
) -> None:
    resp = client.post(
        "/transactions/transfer",
        headers=auth_headers,
        data=json.dumps(
            {
                "account_from": "test_from",
                "account_to": credit_account.name,
                "amount": 100,
            }
        ),
    )
    assert resp.status_code == 400
    assert resp.text == "The account test_from doesn't exist."

    resp = client.post(
        "/transactions/transfer",
        headers=auth_headers,
        data=json.dumps(
            {
                "account_from": debit_account.name,
                "account_to": "test_to",
                "amount": 100,
            }
        ),
    )
    assert resp.status_code == 400
    assert resp.text == "The account test_to doesn't exist."
