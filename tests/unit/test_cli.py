import pytest
from pytest_mock import MockerFixture
from typer import Typer
from typer.testing import CliRunner

from src.cli import init_cli, run_cli


def test_init_cli() -> None:
    cli = init_cli()
    assert isinstance(cli, Typer)


def test_run_cli(mocker: MockerFixture) -> None:
    init_cli = mocker.patch("src.cli.init_cli")
    run_cli()
    init_cli.assert_called_once()


@pytest.mark.parametrize("command", ["run", "r"])
def test_run(
    mocker: MockerFixture,
    runner: CliRunner,
    cli: Typer,
    command: str,
) -> None:
    run_server = mocker.patch("src.cli.app.run_server")

    resp = runner.invoke(cli, [command])
    assert resp.exit_code == 0
    run_server.assert_called_once()
