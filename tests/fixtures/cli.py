import pytest
import typer
from src.cli import init_cli
from typer.testing import CliRunner


@pytest.fixture(scope="session")
def cli() -> typer.Typer:
    return init_cli()


@pytest.fixture(scope="session")
def runner() -> CliRunner:
    return CliRunner()
