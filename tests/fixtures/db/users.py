import pytest

from src.db import User


@pytest.fixture
def user() -> User:
    return User(
        username="test",
        is_admin=False,
    )


@pytest.fixture
def users() -> list[User]:
    return [
        User(
            username=f"test_{i}",
            is_admin=False,
        )
        for i in range(3)
    ]


@pytest.fixture
def admin() -> User:
    return User(
        username="admin",
        is_admin=True,
    )
