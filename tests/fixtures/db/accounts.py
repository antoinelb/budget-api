from datetime import datetime, timedelta

import pytest

from src.db import Account, AccountHistory, User

from .db import *  # noqa
from .users import *  # noqa


@pytest.fixture
async def debit_account(user: User) -> Account:
    return await Account.create(
        user_=user.username, name="debit", is_credit=False, amount=10000
    )


@pytest.fixture
async def credit_account(user: User) -> Account:
    return await Account.create(
        user_=user.username, name="credit", is_credit=True, amount=10000
    )


@pytest.fixture
async def other_debit_account(user: User) -> Account:
    return await Account.create(
        user_=user.username, name="other_debit", is_credit=False, amount=10000
    )


@pytest.fixture
async def other_credit_account(user: User) -> Account:
    return await Account.create(
        user_=user.username, name="other_credit", is_credit=True, amount=10000
    )


@pytest.fixture
async def debit_account_history(
    debit_account: Account,
) -> list[AccountHistory]:
    n = 3
    return [
        await AccountHistory.create(
            debit_account, datetime.utcnow().date() + timedelta(days=-i)
        )
        for i in range(n)
    ]
