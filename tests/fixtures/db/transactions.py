import pytest

from src.db import Account, Transaction

from .accounts import *  # noqa
from .db import *  # noqa


@pytest.fixture
async def transaction(debit_account: Account) -> Transaction:
    return await Transaction.create(
        account=debit_account, item="test", category="test", amount=-10000
    )


@pytest.fixture
async def transactions(
    debit_account: Account, credit_account: Account
) -> list[Transaction]:
    return [  # type: ignore
        *[
            await Transaction.create(
                account=debit_account,
                item=f"test_{i}",
                category="test",
                amount=-100,
            )
            for i in range(3)
        ],
        *[
            await Transaction.create(
                account=credit_account,
                item=f"test_{i}",
                category="test",
                amount=-100,
            )
            for i in range(3)
        ],
    ]
