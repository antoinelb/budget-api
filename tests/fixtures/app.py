from typing import Iterator

import pytest
from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response
from starlette.testclient import TestClient

from src.app import create_app


@pytest.fixture
def app() -> Starlette:
    async def secure_ping(req: Request) -> Response:
        return PlainTextResponse(
            f"Secure pong for user {req.scope['user'].username}!"
        )

    app = create_app()
    app.add_route("/secure", secure_ping, methods=["GET"])
    return app


@pytest.fixture
def client(app: Starlette) -> Iterator[TestClient]:
    with TestClient(app) as client:
        yield client
