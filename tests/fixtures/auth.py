from datetime import datetime, timedelta

import jwt
import pytest

from src import config
from src.db import User

from .db import *  # noqa


@pytest.fixture
def access_token(user: User) -> str:
    payload = {
        "username": user.username,
        "is_admin": False,
        "aud": config.TOKEN_AUD,
        "iat": datetime.utcnow(),
        "exp": datetime.utcnow() + timedelta(minutes=1),
    }
    return jwt.encode(payload, str(config.TOKEN_KEY))


@pytest.fixture
def auth_headers(access_token: str) -> dict[str, str]:
    return {"access-token": access_token}


@pytest.fixture
def admin_auth_headers(admin: User) -> dict[str, str]:
    return {
        "access-token": jwt.encode(
            {
                "username": admin.username,
                "is_admin": True,
                "aud": config.TOKEN_AUD,
                "iat": datetime.utcnow(),
                "exp": datetime.utcnow() + timedelta(minutes=1),
            },
            str(config.TOKEN_KEY),
        )
    }
