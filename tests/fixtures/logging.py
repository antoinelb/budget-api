import logging

import pytest
from _pytest.logging import LogCaptureFixture


@pytest.fixture(autouse=True)
def init_logging(caplog: LogCaptureFixture) -> None:
    caplog.set_level(logging.ERROR)
    caplog.set_level(logging.INFO, "budget")
