from typing import Any, Awaitable, Callable, TypeVar

T = TypeVar("T")


def as_async_fct(fct: Callable[..., T]) -> Callable[..., Awaitable[T]]:
    async def fct_(*args: Any, **kwargs: Any) -> T:
        return fct(*args, **kwargs)

    return fct_


def equals(a: Any, b: Any) -> bool:
    if type(a) != type(b):
        return False

    if isinstance(a, (tuple, list)):
        if len(a) != len(b):
            return False
        else:
            return all(equals(aa, bb) for aa, bb in zip(a, b))

    elif isinstance(a, dict):
        try:
            return all(equals(a[k], b[k]) for k in a.keys())
        except KeyError:
            return False

    else:
        return a == b
